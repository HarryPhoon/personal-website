---
title: Nejsem hobit
draft: false
modified: 2021-05-30T20:50:53+02:00
---

Bohužel nejsem hobit, no. Ale minimálně jsem pohodlný jako oni. Jmenuji se
Ondřej Míchal a jsem student informačních technologií na [FIT VUT](https://www.fit.vut.cz)
Brno. Pracuju jako stážista v [Red Hatu](https://www.redhat.com/en/global/czech-republic) v Desktop týmu, kde zejména plníme roli spolu-správce nástroje [Toolbox](https://github.com/containers/toolbox).

Ve svém volném čase přispívám do různých open source projektů (zejména pod hlavičkou [Fedora](https://getfedora.org)
A [GNOME](https://www.gnome.org)).

Miluju J.R.R.Tolkiena.

PS: Na internetu se objevuju buď jako "Marty", "MartyHarry" nebo "HarryMichal".
