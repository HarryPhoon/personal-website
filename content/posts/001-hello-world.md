---
title: "Hello World"
date: 2020-02-24T11:17:48+01:00
draft: false
---

Hello World! This is my first blog post on my new site.

## What's this place about

Well... this is my home. Here I can smoke Old Toby in my pipe, put my feet
on the table and chat my distant cousine from my mother side...

**Ok, now a bit more seriously.**

This is supposed to be my portfolio and a blog. The posts here will be
mostly technical related to my work or projects I'm currently working on.

See you at the Green Dragon's :)!
