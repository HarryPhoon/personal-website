---
title: "Where's Toolbox? - 0.0.9x update"
date: 2020-07-03T14:41:12+02:00
tags: ["toolbox", "fedora", "silverblue", "OSS"]
draft: false
---

Shortly after the move of [Toolbox](https://github.com/containers/toolbox) to the [containers](https://github.com/containers) organization on GitHub I started working on rewriting Toolbox from Shell to Go (one of goals of my Internship at the Desktop Team at Red Hat). To quickly sum up the reasons for the rewrite:

- Shell is Shell. It is good for small scripts and quick prototyping but that's it. There is no good way to parse JSON (and data overall), have better control over logging or handle errors. The list could go on.

- The "containers world" uses Go. If we want to get their help we need to come to them. Some examples are: [Podman](https://github.com/containers/libpod), [Docker](https://github.com/docker/cli), [Skopeo](https://github.com/containers/skopeo), [Buildah](https://github.com/containers/buildah) or [Kubernetes](https://github.com/kubernetes/kubernetes).

You may ask: "Why didn't you use Python? It's used a lot in Fedora". The answer is: "We can't really." We want Toolbox to be used in Fedora CoreOS which doesn't ship Python. More information about Fedora CoreOS not shipping Python can be found [here](https://github.com/coreos/fedora-coreos-tracker/blob/master/Design.md#approach-towards-shipping-python)

## What has changed?

In terms of functionality not much. Toolbox should behave more-or-less the same as it did before. Most changes are either purely visual, minor or under-the-hood.

**Support for Podman V2**

Developers of Podman never sleep and V2 is an evidence of the fact. Check out their [blog post](https://podman.io/blogs/2020/06/29/podman-v2-announce.html) about the V2 update.

Major updates of software quick often introduce different behaviour and also bugs. Podman V2 was no exception to this rule and some bugs had to be fixed on the part of Podman but in few cases Go version of Toolbox had to be adjusted to work with Podman V2.

> Shell Toolbox (<=v0.0.18) should be compatible with Podman V2 (but expect some changes in formats in some places; e.g., `toolbox list`).

**Deprecation of toolbox containers created with Toolbox 0.0.9 and older**

Toolbox containers created with v0.0.9 or older did not use command `toolbox init-container` as their entry-point.

**Deprecation of the reset command**

Toolbox 0.0.16 added a `reset` command for resetting Podman in case of fatal problems. Podman 1.7.0 added a `system reset` command, so Toolbox's `reset` should no longer be needed.

The command itself can still be used but you'll see a deprecation message.

**Specifying names of containers**

When using commands `enter` and `create` you most of the time need to specify which container you want to use. This has been done using the `--container | -c` option. This is not the way Podman behaves, though. With this update the option was made into an argument of these two commands.

The change does not apply to the `run` command as it's usage is a bit more sophisticated than the previous two.

Here are examples of how to use the commands now:

*Creating a named container*

```shell
[user@hostname]$ toolbox create my-container
Created container: my-container
Enter with: toolbox enter my-container
```

*Entering a named container*

```shell
[user@hostname]$ toolbox enter my-container
[user@toolbox]$
```

**Add handling of error when PWD does not exist in a container**

Toolbox used to fail silently if you tried to enter a toolbox with: `toolbox enter`. Thank you [
José Miguel Parrella](https://github.com/bureado) for reporting and fixing the issue!

https://github.com/containers/toolbox/commit/89ff98d8c3b283b4d74477728eaa39fbe38d3e48

**Aborting container creation**

Shell Toolbox has a problem with interruption of commands that use "spinner" to indicate that work is being done. This was especially noticeable when creating containers (pulling of images). This issue should be gone now.

**Fix duplication of entries when listing toolbox containers**

Due to the way listing of containers in the Shell Toolbox was done there were sometimes cases where a containers with dots in their names (e.g., `my.container`) were showed multiple times. This should occur no more.

**Versioning**

Starting now, Toolbox finally has a `--version` option that shows... the version number.

It may look like this:

```shell
[user@hostname]$ toolbox --version
toolbox version 0.0.91
```

**Logging**

Shell Toolbox used a simple `--verbose` option to toggle verbose logging and a `--very-verbose` toggle to also show the verbose log of Podman. These two options were replaced by two new options: `--log-level` that accepts a string specifying the level of logging ("trace", "debug", "info", "warn", "error", "fatal") and `--log-podman` that exposes logs of Podman on the level that is specified by `--log-level`.

The old options (`--verbose` and `--very-verbose` + their abbreviations `-v` and `-vv`) are kept for backwards compatibility but they are marked as deprecated and in the future they will be dropped completely.

## What's (maybe) coming next?

A large number of issues was reported over the past few months. Most of these issues were left unanswered. With Toolbox being rewritten it should be easier to solve those issues.

Toolbox container set the HOSTNAME variable to `toolbox` but there is no easy to tell what kind of toolbox it is. There are several proposed solutions for this problem. We just need to choose.

We use `flatpak-session-helper` for tracking key system files but it has one problem. It relies on session-bus which is only available with running session. This is a problem when Toolbox is ran with `sudo` or as root. This'll probably require us to write a monitoring tool that does the same as `flatpak-session-helper`but works even with root. *This is a blocker for Toolbox to work in Fedora CoreOS.*

Toolbox has no nice way of telling the user what exactly went wrong when a container couldn't be initialized (it just exits with `failed to start container xxx`). Watching the initialization, reporting back and even providing some help would be very helpful for both us and the users.

> The data need for this can be found in the output of `podman logs <name-of-container>`.

We currently only support our own images for creating containers. Long story short: It would be nice if Toolbox was capable of using even different images. I already saw several examples of how people are trying to solve this problem.

## Where can I get the update?

Check out the [release section](https://github.com/containers/toolbox/releases) of Toolbox's GitHub repo.

### Fedora

Toolbox v0.0.92 is already [available](https://bodhi.fedoraproject.org/updates/FEDORA-2020-dd1372d91d) in Rawhide. If you already have Toolbox installed, just update. If you don't, install it with `dnf install toolbox`.

Toolbox v0.0.92 is also [available](https://bodhi.fedoraproject.org/updates/FEDORA-2020-b703543e25) in *testing* repository of Fedora 32. We encourage you to help us test that release before we push it to stable (and then push it to Fedora 31). To report the issues either use Toolbox's [bug tracker](https://github.com/containers/toolbox/issues) or report it in the update ticket in Bodhi and give it negative karma.

### Arch Linux

Toolbox is [packaged](https://www.archlinux.org/packages/community/x86_64/toolbox/) by the community in Arch Linux and the rewritten version should already be available. Just type `pacman -S toolbox`

### Other distros

I'm not aware of any other distro having Toolbox packaged. If you're interested, we'd very much welcome the possibility of you packaging Toolbox for other distros.

For now you can build Toolbox from source (instructions for doing so can be found in a [PR](https://bodhi.fedoraproject.org/updates/FEDORA-2020-b703543e25) with an update for the projects README).

## Summary

I want to thank all users of Toolbox for reporting bugs and trying to come up with solutions for them. I know this release took a long time but we tried to ensure the quality of the code was as high as possible and we'll continue with this still on our minds.

Also I thank [Debarshi Ray](https://github.com/debarshiray) who's a great colleague and even greater friend who kept answering patiently all my questions, reviewing my patches and reworking my bad commits :P. 

The full list of changes in 0.0.9x can be found in the [changelog](https://github.com/containers/toolbox/blob/master/NEWS) in the project [repository](https://github.com/containers/toolbox)

When the 0.0.9x releases are considered stable enough, we'll probably mark a 0.1 release or maybe even 1.0. We'll see.

In case you experience a bug, report it in Toolbox's [bug tracker](https://github.com/containers/toolbox/issues).