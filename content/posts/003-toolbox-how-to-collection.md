---
title: "A little collection of 'How to do X with Toolbox on Fedora Silverblue'"
date: 2020-05-02T10:14:12+02:00
tags: ["toolbox", "fedora", "silverblue", "OSS"]
draft: false
---

During the past few months I saw several questions (and sometimes even answers to them) about "How to do X with Toolbox?". Some of them have answers, some of them don't. Point is, that these questions have been asked in many places over the internet and it's very hard to look them up.

This article aims to aggregate some of those questions/answers (+ some of my own) and I'll use it as a base for updating the [documentation of Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/). I'll keep updating this post with more QAs as time goes.

> If you have a question about "How to do X with Toolbox" and it's not covered in this post or on the wiki, let me know in the comment section.

With this I want to encourage you, if you have anything that might be worth sharing with the rest of the community, to contribute to the documentation. It's very easy!

To update the docs, you can just go to the page of the documentation you want to change/update. In the upper right corner is a link "Edit this Page" that leads to a GitHub [repository](https://github.com/fedora-silverblue/silverblue-docs). You can start making changes immediately in the text field that you'll get after clicking the link. When you are done, you simply give the update a name and in the description you can describe why the changes were made (if the change is simple/self explaining, then you don't have to write a description).

There's also a [documentation entry](https://docs.fedoraproject.org/en-US/fedora-docs/contributing/) on how to start contributing to Fedora's documentation

> For those who're interested, the software powering Fedora's documentation is [Antora](https://antora.org/).

## How to stop a toolbox?

There is no urgent need for stopping toolboxes. Their memory overhead is very small (about 280Kib while not in interactive session). But if you want to stop them, just run:

```
// To stop a single toolbox
$ podman stop <name-of-the-toolbox>
// To stop all containers
$ podman stop -a
// To stop the latest container
$ podman stop -l
```

You can check, whether they're still running with `toolbox list` (their 'Status' should say "Exited (143) xxx ago" or with `podman ps` (if a container is running, it is on the list).

## How to run a program on the host from a toolbox?
**Inspired by:**

- https://github.com/containers/toolbox/issues/145

While inside of toolbox user can not by default execute programs available only on the host. Thankfully there is a very handy utility, made by folks working on [Flatpak](https://github.com/flatpak/flatpak), called `flatpak-spawn` (link to repo [here](https://github.com/flatpak/flatpak-xdg-utils)).

Long-story-short.. `flatpak-spawn` uses [D-Bus](https://www.freedesktop.org/wiki/Software/dbus/) under the hood to execute commands from a container (or a flatpak) on the host (requires the use of the `--host` option.

You can use `flatpak-spawn` in several different ways:

### Simply running flatpak-spawn`

If you have an urgent need to launch an executable on the host from a container, you can simply type:

```
$ flatpak-spawn --host vim
```

And the host's `vim` will appear in front of you (considering it *is* installed *heh*)

### Using an alias

Previous option is not very practical if you need to do the launching more than once. Solution for that is creating an alias.
It is your choice if you want it permanent or temporary. If just 
temporary, simply type:

```
$ alias vim="flatpak-spawn --host vim"
```

If you want it to be permanent, add the same line to your .bashrc (or different configuration file if you don't use `bash`).

### Creating a shim binary

While making aliases is fine, having an actual file that you can point to and run is more practical (apps ran in a toolbox can use this).

> Toolbox itself currently does not offer an automatic way of creating shim binaries. The issues is being tracked here: https://github.com/containers/toolbox/issues/145

Until Toolbox introduces an official way of handling this situation, it is possible to create these shims manually.

This can be done by either creating a base script and create symbolic links pointing at it or just create scripts for every app you need. The approach with linking is easier to accomplish but the second approach gives you the option to add some special options for your wrappers.

Here's an example of how the base script can look (commission of [Dusty Mabe](https://github.com/dustymabe) - source [here](https://github.com/containers/toolbox/issues/145#issuecomment-582040463)):

```bash
$ cat /usr/local/bin/host-runner 
#!/bin/bash
executable=$(basename $0)
set -x
exec flatpak-spawn --host $executable "$@"
```

The `host-runner` script is placed in `/usr/local/bin` so that it does not interfere with packages installed *in* a toolbox but also with packages installed/layered on the host (tldr; `/usr/local/bin` is exclusive for toolbox).

> You'll need to create the `host-runner` file as root (or with `sudo`).

Here's an example how you can create a link for `vim`:

```
$ sudo ln -s /usr/local/bin/host-runner /usr/local/bin/vim
``` 

> Creating the link also has to be run as root (or with `sudo`).

**Cons:**

- cannot use `sudo`
- `+ exec flatpak-spawn --host vim` is printed in `stderr` after executing (resp. before executing the `flatpak-spawn` sequence)

If you have an idea how to update the script, share it, please!

## How to handle toolbox environment in .bashrc?
**Inspired by:**

- https://discussion.fedoraproject.org/t/install-something-inside-toolbox/17102
- https://discussion.fedoraproject.org/t/how-to-prevent-toolbox-cluttering-host-home-folder/15352/2

On Fedora Silverblue it's quite common that you add an alias to your .bashrc (like `alias vim="flatpak run org.vim.Vim"`). This might cause problems if you install an app you aliased to your toolbox container.

The error could look like this:
```
/var/lib/flatpak/exports/bin/org.vim.Vim: line 2: /usr/bin/flatpak: No such file or directory
```

To prevent this from happening, you can make use of `/run/.containerenv` and `/run/.toolboxenv`. `.containerenv` is created by Podman/Docker and `.toolboxenv` is created by Toolbox in containers. So if `/run/.toolboxenv` exists, you are in a toolbox.

To make use of it you can update your `.bashrc` (or it's section) to look like this:
```bash
if ! [ -f /run/.containerenv ] || ! [ -f /run/.toolboxenv ]; then
	# this IS NOT executed in any container
	alias vim="flatpak run org.vim.Vim"
fi

if [ -f /run/.containerenv ] && [ -f /run/.toolboxenv ]; then
	# this IS executed ONLY in a toolbox
	alias vim="flatpak-spawn --host vim"
fi
```

## How to ssh into a toolbox from VS Code?
**Inspired by:**

- https://discussion.fedoraproject.org/t/ssh-into-a-toolbox/2155

Until VS Code lands [full support](https://github.com/microsoft/vscode-remote-release/issues/116) for [Podman](https://podman.io/) alongside Docker ('[Remote - containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)' extension), ssh-ing into a toolbox is the closest to the final experience.

NOTE: Looks like support for Podman will come to VS Code [soon](https://github.com/microsoft/vscode-remote-release/issues/116#issuecomment-620477964)!!

Here's an excellent [blog post](https://jurf.github.io/2020/03/24/vscode-toolbox/) written by [Juraj Fiala](https://jurf.github.io/about/) where he covers all the steps to setup a openssh server *in* a toolbox and connect to it from VSCode using the '[Remote - SSH]' extension.

## How to get toolbox's name from within a toolbox?
**Inspired by:**

- https://discussion.fedoraproject.org/t/get-container-name-from-within-container/18073

There isn't currently a reliable to get toolbox's name from within it.

One thing you can do is run:

```
$ flatpak-spawn --host podman ps --format "{{ .Names }}"
```

which will show names of *all* currently running containers. But this has the problem with showing more than one container.

## How to increase allowed file system watchers in a toolbox?
**Inspired by:**

- https://discussion.fedoraproject.org/t/how-do-i-increase-the-number-of-allowed-file-system-watchers-inside-a-toolbox-container/14523

Increasing the number of allowed file system watchers has to be done on the host. Toolboxes will inherit this setting.


## How do I create a toolbox with its own home directory?
**Inspired by:**

- https://discussion.fedoraproject.org/t/is-it-possible-to-create-a-toolbox-with-its-own-separate-home-directory/14736
- https://github.com/containers/toolbox/issues/348

There is currently no official way to create a toolbox with a different home folder than the current user's. But there are at least two temporary solutions that you can use (both were inspired by [yilkalargaw](https://github.com/yilkalargaw).

### Override `HOME` environmental variable

It is possible to create a toolbox with a different home folder by overriding the `HOME` variable. The catch is that it causes both Toolbox and Podman operate in that environment. That'll cause Podman to start downloading the required image again.

Also interacting with the toolbox requires you to override the `HOME` variable every time. So setting and alias is highly encouraged.

Workflow could look like this (considering `/home/user/Projects/example` is an existing directory):

```
$ HOME=/home/user/Projects/example toolbox create -y example
$ alias toolbox-example="HOME=/home/user/Projects/example"
$ toolbox-example toolbox enter example
[user@toolbox] $
```

### Use direnv

[direnv](https://direnv.net/) is a tool for loading/unloading environment variable depending on the directory you're in. This requires you to first install and setup `direnv` and then write a `.envrc` file that suits your needs.

## How to create a .desktop file for a program in a toolbox?
**Inspired by:**

- https://discussion.fedoraproject.org/t/vscode-running-on-toolbox-via-desktop-launcher-but-icons-are-duplicated-on-dock/18899
- https://github.com/containers/toolbox/issues/417

Toolbox does not offer a way to create a .desktop file for an app in a toolbox.

The main difference between a "normal" .desktop file and a toolbox one is the `Exec` entry. To execute an app in toolbox you need to prepend `toolbox run <container> <executable>`.

Example (toolbox with the app is called 'desktop-apps'):
```
// Before
...
Exec=simple-scan
...
// After
...
Exec=toolbox run desktop-apps simple-scan
...
```

## How do I set a default shell in a toolbox?
**Inspired by:**

- https://github.com/containers/toolbox/issues/413

Toolbox mirrors the shell used in a toolbox with the one used on the host. So if you use `/bin/bash` then that's what willb be used in a toolbox. You can change this by overriding the `SHELL` environmental variable.

Example (open a toolbox with `/bin/sh` shell):

```
$ SHELL=/bin/sh toolbox enter
```

## How do I create a rootful toolbox?
**Inspired by:**

- https://github.com/containers/toolbox/issues/267

Toolbox currently works only in rootless mode. The main problem is that Toolbox relies on [`flatpak-session-helper`](https://github.com/flatpak/flatpak/tree/master/session-helperx) for monitoring crucial host files (host.conf, hosts, localtime, resolv.conf, timezone). It is called over [D-Bus](https://www.freedesktop.org/wiki/Software/systemd/dbus/). The problem with D-Bus is, that it relies on X11 display being present which is not the case for `root` user.

There is currently no workaround.

To solve this we need to solve how to launch `flatpak-session-helper` without D-Bus or write our own helper that works in both modes.
