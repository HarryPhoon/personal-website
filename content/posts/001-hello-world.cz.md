---
title: "Ahoj Světe"
date: 2020-02-24T11:17:48+01:00
draft: false
---

Ahoj Světe! Tohle je můj první příspěvek na nové stránce.

## O čem to tady je

No... Tohle je můj domov. Tady si můžu v klidu a míru nacpat do dýmky Starého
Tobyho, dát si nohy na stůl a v konverzovat o bratranci z pátého kolene z
matčiny strany...

**Fajn, teď trochu vážně.**

Tohle má být něco jako moje portfolio a blog. Příspěvky budou asi převážně
technické. Něco o práci, něco o mých projektech a tak..

Uvidíme se U Zeleného draka :)!


