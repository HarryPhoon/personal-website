---
created: 2021-06-26T18:52:07+02:00
title: Chytré telefony, internet a pohromy
draft: false
tags: [blábolení]
modified: 2021-06-27T14:43:05+02:00
---

Téměř každý zaznamenal, co se událo během posledních pár dnů. Jižní Moravu zasáhla série krupobití, přívalových dešťů, prudkého větru a na některých místech tornáda. Ty si vyžádaly smrt několika lidí, někteří bojují o život a mnozí další byli zraněni. Hmotné ztráty nebudu ani zmiňovat. Neznám přesná čísla, ale vím, že jsou příšerná.

V článku, vydaném v Respektu, mě jako člověka, který by se nazval technologickým konzervativcem, velmi zaujalo, jak autorka vyzdvihla, jaké problémy mají lidé v postižených oblastech s nabíjením svých mobilních zařízení, s přístupem k mobilním sítím (ať už pro volání nebo přístup k internetu) nebo s obecným přístupem k internetu. Svým způsobem tento fakt potvrzuje mé vnitřní přesvědčení, že lidé až nezdravé spoléhají na moderní technologie, které nejsou navrženy způsobem, který by byl odolný proti náhlým a závažným (nejen) živelným situacím.

Mobilní telefony, nebo dnes spíše chytré telefony, vyžadují téměř denní nabíjení. Některé modely zvládnou i více dnů, ale „pravidlo palce” je spíše, že se telefon nabíjí každý večer. Při ztrátě elektrického spojení člověk bolestivě zjistí, jak žalostně krátká životnost telefonu je.

Mobilní sítě a internet jdou mnohdy ruku v ruce. Pokud k nim člověk ztratí přístup, zjistí, že jeho zařízení téměř nic užitečného neumí a není mu nic co platné.

Tímto textem se nesnažím říct, že používání chytrých telefonů a jakýchkoliv služeb, závislých na přístupu k internetu, je špatné. Naopak! Dokážou být nadmíru užitečné a v minulosti se už několikrát prokázaly být klíčovými. Spíše se snažím upozornit na naše přílišné spoléhání na tyto technologie. Bereme je jako samozřejmé, ale situace jako ty, které teď zasáhly mnoho lidí nejen na Hodonínsku, jsou důkazem, že tomu tak není.

Článek napsán v reakci na [článek](https://www.respekt.cz/agenda/prekracuji-strechy-zdivo-kusy-aut-chvali-hasice-i-zachranare-preji-si-stesti?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+odemcene-clanky+%28T%C3%BDden%C3%ADk+Respekt+%E2%80%A2+V%C5%A1echny+odem%C4%8Den%C3%A9+%C4%8Dl%C3%A1nky%29) paní Ivany Svobodové v Respektu
